"""Tests for the local compound cache."""

# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from pathlib import Path

import pandas as pd
import pytest

from equilibrator_assets.local_compound_cache import LocalCompoundCache

from . import chemaxon, noInChI, winOS


@pytest.fixture(scope="function")
def local_cache() -> LocalCompoundCache:
    """Create a local cache."""
    local_cache = LocalCompoundCache()
    local_cache.generate_local_cache_from_default_zenodo(
        "./compounds.sqlite", force_write=True
    )
    local_cache.load_cache("./compounds.sqlite")
    yield local_cache
    # Delete copy of .sqlite file
    local_cache.ccache.session.close()
    local_cache.ccache_path.unlink()


def test_generate_cache_from_default_zenodo() -> None:
    """Test copying the default cache."""
    local_cache = LocalCompoundCache()
    local_cache.generate_local_cache_from_default_zenodo(
        "compounds.sqlite", force_write=True
    )

    assert Path("compounds.sqlite").is_file()
    # Remove copied database
    Path("compounds.sqlite").unlink()


@chemaxon
def test_get_compounds_from_smiles(local_cache) -> None:
    """Get a mix of compounds that exist and are not in the ccache."""
    smiles = [
        "CC(=O)[O-]",  # Acetate in ccache
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",  # 3B4HA Not in ccache
    ]
    cpd_results = local_cache.get_compounds(smiles)

    assert cpd_results[0].compound.id == 28
    assert cpd_results[1].compound.id == 694325

    assert cpd_results[0].compound.dissociation_constants == [4.54]
    assert cpd_results[1].compound.dissociation_constants == [8.92, 4.23]

    assert len(cpd_results[0].compound.magnesium_dissociation_constants) == 1
    assert len(cpd_results[1].compound.magnesium_dissociation_constants) == 0


@chemaxon
def test_get_single_compound_from_smiles(local_cache) -> None:
    """Get a single compound."""
    smiles = "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1"  # 3B4HA Not in ccache
    cpd_results = local_cache.get_compounds([smiles])

    assert cpd_results[0].compound.id == 694325
    assert cpd_results[0].compound.dissociation_constants == [8.92, 4.23]


@noInChI
@chemaxon
def test_get_compounds_from_inchi(local_cache) -> None:
    """Get a mix of compounds that exist and are not in the ccache."""
    inchi = [
        "InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1",  # Acetate--In ccache
        (
            "InChI=1S/C14H11NO4"
            "/c16-12-7-6-10(14(18)19)8-11(12)15-13(17)9-4-2-1-3-5-9"
            "/h1-8,16H,(H,15,17)(H,18,19)"
        ),  # 3B4HA--Not in ccache
    ]
    cpd_results = local_cache.get_compounds(inchi, mol_format="inchi")

    assert cpd_results[0].compound.id == 28
    assert cpd_results[1].compound.id == 694325

    assert cpd_results[0].compound.dissociation_constants == [4.54]
    assert cpd_results[1].compound.dissociation_constants == [9.54, 5.49, 4.62]

    assert len(cpd_results[0].compound.magnesium_dissociation_constants) == 1
    assert len(cpd_results[1].compound.magnesium_dissociation_constants) == 0


@chemaxon
def test_get_compounds_specifying_pkas(local_cache) -> None:
    """Specify dissociation constants of a compound."""
    smiles = [
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",  # 3B4HA Not in ccache
        "OCC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
    ]
    pka_dict = {"OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1": [4, 5.5, 6]}
    cpd_results = local_cache.get_compounds(smiles, specified_pkas=pka_dict)

    assert cpd_results[0].compound.dissociation_constants == [4, 5.5, 6]
    assert cpd_results[1].compound.dissociation_constants == [13.86, 6.97]


@chemaxon
def test_get_compounds_with_errors(local_cache):
    """Call get_compounds with structures that have errors and test logs."""
    smiles = [
        # Decomposed with chemaxon
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        # Decomposed with bypass_chemaxon
        "C1(CC(OC(C(C1)=O)CO)O)=O",
        # Cannot be decomposed
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
    ]
    cpd_results = local_cache.get_compounds(smiles)

    assert len(cpd_results) == 3
    assert cpd_results[0].structure == "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1"
    assert cpd_results[0].inchi_key == "RKCVLDMDZASBEO-UHFFFAOYSA-N"
    assert cpd_results[0].method == "chemaxon"
    assert cpd_results[0].status == "valid"
    assert cpd_results[0].compound is not None
    assert cpd_results[0].compound.id == 694325
    assert cpd_results[0].compound.dissociation_constants == [8.92, 4.23]

    assert cpd_results[1].structure == "C1(CC(OC(C(C1)=O)CO)O)=O"
    assert cpd_results[1].inchi_key == "UBJAOLUWBPQQJC-UHFFFAOYSA-N"
    assert cpd_results[1].method == "chemaxon"
    assert cpd_results[1].status == "valid"
    assert cpd_results[1].compound is not None
    assert cpd_results[1].compound.id == 694326
    assert cpd_results[1].compound.dissociation_constants == [12.34, 5.67]

    assert cpd_results[2].structure == "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC"
    assert cpd_results[2].inchi_key == "DNOMLUPMYHAJIY-UHFFFAOYSA-N"
    assert cpd_results[2].method == "empty"
    assert cpd_results[2].status == "failed"
    assert cpd_results[2].compound is None


@chemaxon
def test_get_compounds_bypass_chemaxon(local_cache):
    """Test generating cpd_results by bypassing chemaxon."""
    smiles = [
        # Decomposed with chemaxon
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        # Decomposed with bypass_chemaxon
        "C1(CC(OC(C(C1)=O)CO)O)=O",
        # Cannot be decomposed
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
    ]

    cpd_results = local_cache.get_compounds(smiles, bypass_chemaxon=True)

    assert cpd_results[0].compound.group_vector
    assert (
        cpd_results[0].compound.smiles == "OC1=C(NC(=O)C2=CC=CC=C2)C=C(C=C1)C([O-])=O"
    )

    assert cpd_results[1].compound.group_vector
    assert cpd_results[1].compound.smiles == "OCC1OC(O)CC(=O)C=C1[O-]"

    assert cpd_results[2].compound is None


@chemaxon
def test_get_compounds_save_empty(local_cache):
    """Test saving empty cpd_results."""
    smiles = [
        # Decomposed with chemaxon
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        # Decomposed with bypass_chemaxon
        "C1(CC(OC(C(C1)=O)CO)O)=O",
        # Cannot be decomposed
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
    ]

    cpd_results = local_cache.get_compounds(
        smiles, bypass_chemaxon=False, save_empty_compounds=True
    )

    assert cpd_results[0].compound.group_vector
    assert (
        cpd_results[0].compound.smiles == "OC1=C(NC(=O)C2=CC=CC=C2)C=C(C=C1)C([O-])=O"
    )

    assert cpd_results[1].compound.group_vector
    assert cpd_results[1].compound.smiles == "OCC1OC(O)CC(=O)C=C1[O-]"

    assert not cpd_results[2].compound.group_vector
    assert (
        cpd_results[2].compound.smiles
        == "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC"
    )


@winOS
def test_get_compounds_read_only(local_cache) -> None:
    """Test read only mode of local cache."""
    local_cache.read_only = True
    smiles = [
        "CC(=O)[O-]",  # Acetate in ccache
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",  # 3B4HA Not in ccache
    ]

    cpd_results = local_cache.get_compounds(smiles)

    # Returns valid, known compound
    assert len(cpd_results) == 2
    assert cpd_results[0].compound.smiles == "CC([O-])=O"

    # Second compound fails due to read-only
    assert cpd_results[1].method == "read-only"


@chemaxon
def test_add_compounds(local_cache) -> None:
    """Add compounds with different values provided."""
    compound_df = pd.DataFrame(
        data=[
            ["OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", "3B4HA", "test_name"],
            ["CCO", None, "EtOH"],
            ["OC(=O)C1=CCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", "test_cpd", None],
            ["OC(=O)C1=CCCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", None, "test_cpd2_name"],
            ["OC(=O)C1=CCCCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", None, None],
        ],
        columns=["struct", "coco_id", "name"],
    )

    cpd_results = local_cache.add_compounds(compound_df)

    assert len(cpd_results) == 5
    assert cpd_results[0].compound.identifiers[0].registry.namespace == "coco"
    assert cpd_results[0].compound.identifiers[0].accession == "3B4HA"
    assert cpd_results[0].compound.identifiers[1].registry.namespace == "synonyms"
    assert cpd_results[0].compound.identifiers[1].accession == "test_name"
    assert cpd_results[0].compound.get_common_name() == "test_name"

    assert cpd_results[1].compound.identifiers[-2].registry.namespace == "synonyms"
    assert cpd_results[1].compound.identifiers[-2].accession == "etoh"
    assert cpd_results[1].compound.identifiers[-1].registry.namespace == "coco"
    assert cpd_results[1].compound.identifiers[-1].accession == "EtOH"
    assert cpd_results[1].compound.get_common_name() == "Ethanol"

    assert len(cpd_results[2].compound.identifiers) == 2
    assert cpd_results[2].compound.identifiers[0].registry.namespace == "coco"
    assert cpd_results[2].compound.identifiers[0].accession == "test_cpd"
    assert cpd_results[2].compound.get_common_name() == "test_cpd"

    assert cpd_results[3].compound.identifiers[0].registry.namespace == "coco"
    assert cpd_results[3].compound.identifiers[0].accession == "test_cpd2_name"
    assert cpd_results[3].compound.get_common_name() == "test_cpd2_name"

    assert cpd_results[4].compound.identifiers[0].registry.namespace == "synonyms"
    assert cpd_results[4].compound.get_common_name() == "694328"


@winOS
def test_add_compounds_read_only(local_cache) -> None:
    """Test adding compounds in read-only mode."""
    local_cache.read_only = True
    compound_df = pd.DataFrame(
        data=[
            ["OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", "3B4HA", "testme"],
            ["CCO", None, "EtOH"],
        ],
        columns=["struct", "coco_id", "name"],
    )
    cpd_results = local_cache.add_compounds(compound_df)

    # in read-only mode, new compounds should not be created
    assert cpd_results[0].compound is None

    assert cpd_results[1].compound is not None
    assert cpd_results[1].compound.identifiers[-1].registry.namespace == "coco"
    assert cpd_results[1].compound.identifiers[-1].accession == "EtOH"


@chemaxon
def test_local_cache_persistence(local_cache) -> None:
    """Test additions to ccache persist upon reloading."""
    smiles = "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1"  # 3B4HA Not in ccache

    local_cache.get_compounds([smiles])

    # Disconnect and reload to the database
    local_cache.load_cache(local_cache.ccache_path)
    cpd_results = local_cache.get_compounds([smiles])

    # ID of first inserted compound
    assert cpd_results[0].compound.id == 694325


@winOS
def test_get_coco_accessions(local_cache) -> None:
    """Get accessions from coco namespace."""
    accessions = local_cache.get_coco_accessions()

    assert len(accessions) == 13
