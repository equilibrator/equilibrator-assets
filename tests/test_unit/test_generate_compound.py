"""Tests for generating compounds."""

# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from equilibrator_assets.generate_compound import get_or_create_compounds

from . import chemaxon, noInChI, winOS


@noInChI
@winOS
def test_get_from_inchis(comp_contribution) -> None:
    """Create a small compound table for testing ChemAxon and OpenBabel."""
    inchis = [  # adenine
        "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
        "InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1",  # acetate
        "InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H",  # benzene
    ]
    cpd_results = get_or_create_compounds(
        comp_contribution.ccache, inchis, mol_format="inchi"
    )

    assert cpd_results[0].compound.dissociation_constants == [9.84, 2.51]
    assert cpd_results[1].compound.dissociation_constants == [4.54]
    assert cpd_results[2].compound.dissociation_constants == []

    assert len(cpd_results[0].compound.magnesium_dissociation_constants) == 2
    assert len(cpd_results[1].compound.magnesium_dissociation_constants) == 1
    assert len(cpd_results[2].compound.magnesium_dissociation_constants) == 0

    assert cpd_results[0].compound.microspecies[0].charge == -1
    assert cpd_results[1].compound.microspecies[0].charge == -1
    assert cpd_results[2].compound.microspecies[0].charge == 0


@chemaxon
def test_create_from_smiles(comp_contribution) -> None:
    """Create a compound that is not in the cache."""
    smiles = [
        "OCC(N)C(O)CO",  # 3-Aminobutane-1,2,4-triol
        "CCCOP(=O)(O)O",  # propyl-phosphate
    ]
    cpd_results = get_or_create_compounds(
        comp_contribution.ccache,
        smiles,
        mol_format="smiles",
    )

    assert cpd_results[0].compound.dissociation_constants == [13.69, 8.92]
    assert cpd_results[1].compound.dissociation_constants == [6.84, 1.82]
    for c in cpd_results:
        assert c.compound.id < 0
        assert len(c.compound.magnesium_dissociation_constants) == 0
        assert c.compound.group_vector is not None
