# The MIT License (MIT)
#
# Copyright (c) 2023 The Weizmann Institute of Science.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from sys import platform
import pytest
from equilibrator_assets.chemaxon import get_chemaxon_status

CHEMAXON_STATUS = get_chemaxon_status()
__cxcalc = pytest.mark.skipif(CHEMAXON_STATUS == 1, reason="cxcalc is not installed.")
__license = pytest.mark.skipif(
    CHEMAXON_STATUS == 2, reason="cxcalc found no ChemAxon license."
)
noInChI = pytest.mark.skipif(
    platform in {"darwin", "win32"}, reason="InChI not supported on macOS and Windows."
)
winOS = pytest.mark.skipif(
    platform == "win32", reason="Windows does not allow multiple accesses to the same file (even read-only)."
)

def chemaxon(f):
    """Compose chemaxon requirements into one decorator."""
    return __cxcalc(__license(f))
