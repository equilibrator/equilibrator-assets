"""Tests for ChemAxon."""

# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import numpy as np
import pandas as pd
import pytest

from equilibrator_assets.chemaxon import (
    get_atom_bag,
    get_dissociation_constants,
    get_molecular_masses,
)

from . import chemaxon, noInChI


@pytest.fixture(scope="module")
def molecules() -> pd.DataFrame:
    """Create a small compound table for testing ChemAxon and OpenBabel."""
    inchis = pd.Series(
        [  # adenine
            "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
            "InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1",  # acetate
            "InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H",  # benzene
        ]
    )
    inchis.name = "inchi"

    res = pd.DataFrame(data=inchis)
    res.index.name = "id"
    return res.reset_index()


@pytest.mark.parametrize(
    "smiles, expected_atom_bag",
    [
        ("CC=O", {"C": 2, "O": 1, "H": 4, "e-": 24}),
        ("C#[N-]", {"C": 1, "H": 1, "N": 1, "e-": 15}),
    ],
)
def test_atom_bag(smiles, expected_atom_bag):
    """Test the OpenBabel based calculation of atom bag and net charge."""
    atom_bag = get_atom_bag("smi", smiles)
    assert atom_bag == expected_atom_bag


@noInChI
@chemaxon
def test_molecular_mass(molecules):
    """Test the ChemAxon-based calculation of molecular mass."""
    result = get_molecular_masses(molecules, "/tmp/test_chamaxon1")
    assert result.mass.iat[0] == pytest.approx(135.1, rel=1e-3)
    assert result.mass.iat[1] == pytest.approx(59.0, rel=1e-3)
    assert result.mass.iat[2] == pytest.approx(78.1, rel=1e-3)


@noInChI
@chemaxon
def test_dissociation_constants(molecules):
    """Test the ChemAxon-based calculation of dissociation constants."""
    result, pka_columns = get_dissociation_constants(
        molecules, "/tmp/test_chamaxon2", num_acidic=2, num_basic=2, mid_ph=7.0
    )
    assert result.major_ms.iat[1] == "CC([O-])=O"

    pkas = result[pka_columns].apply(pd.to_numeric).apply(np.nan_to_num).values
    assert pkas[0, :] == pytest.approx([9.84, 19.61, 1.67, -4.63], rel=1e-3)  # adenine
    assert pkas[1, :] == pytest.approx([4.54, 0, 0, 0], rel=1e-3)  # acetate
    assert pkas[2, :] == pytest.approx([0, 0, 0, 0], rel=1e-3)  # benzene
